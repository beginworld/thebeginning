# thebeginning

We are learning to build our own websites using BeginBot's limitless wisdom. As The Word says, first thing, "In the Beginning, God created the heavens and the earth." And so you see, Begin had been there since the dawn of creation. 0=1; ad infinitum.
